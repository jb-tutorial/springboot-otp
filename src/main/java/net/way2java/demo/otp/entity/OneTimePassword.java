package net.way2java.demo.otp.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter @Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class OneTimePassword {

    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private Integer oneTimePasswordCode;

    @NonNull
    private Date expires;
}
