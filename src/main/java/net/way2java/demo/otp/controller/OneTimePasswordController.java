package net.way2java.demo.otp.controller;

import lombok.RequiredArgsConstructor;
import net.way2java.demo.otp.entity.OneTimePassword;
import net.way2java.demo.otp.service.OneTimePasswordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/otp")
@RequiredArgsConstructor
public class OneTimePasswordController {

    private final OneTimePasswordService oneTimePasswordService;

    @GetMapping("/create")
    public ResponseEntity<OneTimePassword> getOneTimePassword() {
        return ResponseEntity.ok(oneTimePasswordService.returnOneTimePassword());
    }
}
