package net.way2java.demo.otp.service;

import net.way2java.demo.otp.entity.OneTimePassword;
import net.way2java.demo.otp.repository.OneTimePasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OneTimePasswordService {
    private static final Long expiryInterval = 5L * 60 * 1000;

    OneTimePasswordRepository oneTimePasswordRepository;

    @Autowired
    public OneTimePasswordService(OneTimePasswordRepository oneTimePasswordRepository) {
        this.oneTimePasswordRepository = oneTimePasswordRepository;
    }

    public OneTimePassword returnOneTimePassword() {
        var oneTimePassword = new OneTimePassword();

        oneTimePassword.setOneTimePasswordCode(
                OneTimePasswordHelper.createRandomOnetimePassword().get()
        );
        oneTimePassword.setExpires(new Date(System.currentTimeMillis()+ expiryInterval));

        oneTimePasswordRepository.save(oneTimePassword);

        return oneTimePassword;
    }
}
