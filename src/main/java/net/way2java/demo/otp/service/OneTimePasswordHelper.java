package net.way2java.demo.otp.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.function.Supplier;

public class OneTimePasswordHelper {
    private static final Integer LENGTH = 6;

    private OneTimePasswordHelper() {}

    public static Supplier<Integer> createRandomOnetimePassword() {
        return () -> {
            SecureRandom random = null;

            try {
                random = SecureRandom.getInstanceStrong();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            var oneTimePassword = new StringBuilder();

            for(int i =0; i < LENGTH; i++) {
                assert random != null;
                int randomNumber = random.nextInt(10);
                oneTimePassword.append(randomNumber);
            }

            return Integer.parseInt(oneTimePassword.toString().trim());
        };
    }
}
