package net.way2java.demo.otp.repository;

import net.way2java.demo.otp.entity.OneTimePassword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OneTimePasswordRepository extends CrudRepository<OneTimePassword, Long> {}
